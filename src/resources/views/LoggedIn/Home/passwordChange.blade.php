@extends('Layouts.Application')

@section('MainContent')


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><strong> Change Your Password</strong></div>
            <div class="panel-body">
             @include('Layouts.FormValidationErrors')
                 @include('Layouts.ErrorSuccessAndWarninMessages')
                <form class="form-horizontal" role="form" method="POST" action="{{ url(route('LoggedIn.Home.updatePasswordAction')) }}">
                    {{ csrf_field() }}

                    <div class="form-group form-group-sm">
                        <label for="oldpassword" class="col-sm-3 control-label">Current  Password <sup><i class="fa fa-asterisk"style="color:red"></i></sup></label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" id="oldpassword" name="oldpassword" Placeholder="Current  Password"/>
                        </div>
                       
                    </div>
					<div class="form-group form-group-sm">
                        <label for="password" class="col-sm-3 control-label">New Password <sup><i class="fa fa-asterisk" style="color:red"></i></sup></label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" id="password" name="password" Placeholder="New Password"/>
                        </div>
                      
                    </div>
					<div class="form-group form-group-sm">
                        <label for="cnfpassword" class="col-sm-3 control-label">Conform Password <sup><i class="fa fa-asterisk" style="color:red"></i></sup></label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" id="cnfpassword" name="cnfpassword" Placeholder="Conform Password"/>
                        </div>
                      
                    </div>
					 
					 
       

                    <div class="form-group form-group-sm">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success btn-sm">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection