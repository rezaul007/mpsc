
@extends('Layouts.Application')
<?php
//echo '<pre>';print_r($data);echo '</pre>';exit();
?>

@section('MainContent')


<div class="row">
    <div class="col-sm-12">
     
        <div class="panel panel-default">
		@include('Layouts.FormValidationErrors')
                @include('Layouts.ErrorSuccessAndWarninMessages')
            <div class="panel-heading" style="text-align: center"><h3> Online Information </h3></div>
            <div class="panel-body">

             	 <div class="media">
				  <div class="media-left">
					
					  <img class="img-circle" src="{{ asset('img/ProfilePicture') . '/' . Auth::user()->image }}"
                           alt="{{ Auth::user()->image }}" class="img-responsive" width="200" height="150"/>
					
					<b><a href="#" data-toggle="modal" data-target="#myModal"> Edit Picture </a></b>
				  </div>
				  <div class="media-body">
					<h4 class="media-heading">{{Auth::user()->full_name}}</h4>
								
						<!--<form class="form-horizontal">-->
						  
							<div class="form-group">
							
								<label for="email" class="col-sm-4 control-label">E-mail </label>
								<div class="col-sm-6">
								   <p class="form-control-static">{{Auth::user()->email}}</p>
								</div>
								<label for="username" class="col-sm-4 control-label">Username </label>
								<div class="col-sm-6">
								  <p class="form-control-static">{{Auth::user()->username}}</p>
								</div>
							
								<label for="email" class="col-sm-4 control-label">Phone No.  </label>
								<div class="col-sm-6">
								  <p class="form-control-static">{{Auth::user()->phone_no}}</p> 
								</div>		
                                <label for="email" class="col-sm-4 control-label">Phone No.  </label>
								<div class="col-sm-6">
								  <p class="form-control-static">{{Auth::user()->phone_no}}</p> 
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-sm-10">
								  <a href="{{ url(route('LoggedIn.Home.updateOwnProfile')) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Update Profile</a>&nbsp;&nbsp;	  
								  <a href="{{ url(route('LoggedIn.Home.updateProfilePassword')) }}" class="btn btn-primary btn-sm"><i class="fa fa-key"></i> Change Password</a>  
								</div>
							</div>
							
						
					   <!--</form>-->											
				  </div>
				</div>

        </div>

   

</div>	
</div>
</div>
<!-- Modal content-->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Change Your Profile Picture </h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" role="form" method="POST" action="{{url(route('LoggedIn.Home.updateProfile'))}}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                  <div class="form-group form-group-sm">
                        <label for="image" class="col-sm-3 control-label">Select image </label>
                        <div class="col-sm-6">
                             <input type="file" name="image" id="image" value="{{ old('image') }}" />
                        </div>
                        @if ($errors->has('image'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('image') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group form-group-sm">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success btn-sm">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                Save
                            </button>
                        </div>
                    </div>
                </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
   </div>
  </div>

@endsection